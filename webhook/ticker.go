package webhook

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

/*
UpdateRates Function that gets latest rates from fixer,
checks if data from that day is present allready in db
if necessary call to notify registered webhooks
*/
func UpdateRates() (string, error) {
	lastUpdated, err := getLatestRatesFromDB()
	if err != nil {
		// not able to get recent db data
		return "", err
	}

	reqURL := "http://api.fixer.io/latest?base=EUR"

	var currentRates Rates
	// based on https://golang.org/pkg/net/http/#Response
	res, err3 := http.Get(reqURL)
	if err3 != nil {
		//log.Println(err3)
		return "", err3 //
	}
	JSONdata, err4 := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err4 != nil {
		//log.Println(err4)
		return "", err4 //
	}

	json.Unmarshal(JSONdata, &currentRates) // get current rates
	currentRates.TimeStamp = time.Now()

	// ? check if not updated allready today?

	if strings.Compare(lastUpdated.Date, currentRates.Date) == 0 {
		// allready updated today and NOT forced update, no need for update???
		return "Allready newest rates in db", nil
	}
	// try to insert into db
	msgDB, err5 := addLatestRatesToDB(currentRates)
	if err5 != nil {
		return "", err5
	}
	// succesfully updated
	// invoke relevant webhooks
	msgInvoke, err6 := invokeWebhooks(currentRates, false)
	if err6 != nil {
		return msgDB + "\n!! failed on invoking webhooks !! \n" + msgInvoke, err6
	}

	return msgDB + "\n" + msgInvoke, nil
}

package webhook

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

func TestSubscribeAndHandleWebhook(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(HandleSubscribe))
	tsDelAndView := httptest.NewServer(http.HandlerFunc(HandleWebhook))

	submitData := SubscribePayload{"testURL", "EUR", "NOK",
		float32(8), float32(9)}

	jsonStr, _ := json.Marshal(submitData)

	// based on https://stackoverflow.com/questions/24455147/how-do-i-send-a-json-string-in-a-post-request-in-go
	req, err := http.NewRequest("POST", ts.URL, bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Error(err)
		return
	}
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Error(err)
		return
	}
	defer resp.Body.Close()

	fmt.Println("POST subscribe response Status:", resp.Status) // log
	//fmt.Println("response Headers:", resp.Header)		// log
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("POST subscribe response Body:", string(body)) // log
	// end of code from stackoverflow

	if resp.StatusCode == http.StatusBadRequest { // case when all communication is ok
		t.Error(errors.New("Rejected payload")) // but not correct payload format
	}
	// handle registered webhook
	fmt.Println(tsDelAndView.URL)
	webhookID := string(body)
	tsDelAndView.URL = tsDelAndView.URL + "/exchange/" + webhookID

	fmt.Println(tsDelAndView.URL)

	resp2, err2 := http.Get(tsDelAndView.URL)
	if err2 != nil {
		t.Error(err2)
	}

	if resp2.StatusCode == http.StatusBadRequest {
		t.Error(errors.New("Handle GET request rejected"))
	}

	fmt.Println("Handle GET response Status:", resp2.Status)

	// based on https://stackoverflow.com/questions/24455147/how-do-i-send-a-json-string-in-a-post-request-in-go
	req2, err3 := http.NewRequest("DELETE", tsDelAndView.URL, nil)
	if err3 != nil {
		t.Error(err3)
		return
	}

	client2 := &http.Client{}
	resp4, err4 := client2.Do(req2)
	if err4 != nil {
		t.Error(err4)
		return
	}
	defer resp4.Body.Close()

	fmt.Println("DELETE subscribe response Status:", resp4.Status) // log
	//fmt.Println("response Headers:", resp.Header)		// log
	body4, _ := ioutil.ReadAll(resp4.Body)
	fmt.Println("DELETE subscribe response Body:", string(body4)) // log
	// end of code from stackoverflow

	if resp4.StatusCode == http.StatusBadRequest { // case when all communication is ok
		t.Error(errors.New("Rejected DELETE")) // but not correct payload format
	}

}

func TestInvoke(t *testing.T) {
	testWebhookServer := httptest.NewServer(http.HandlerFunc(handleInvoveTest))

	webhook := Webhook{"testID", testWebhookServer.URL, "EUR", "NOK",
		float32(8), float32(9)}

	fmt.Println("Invoking reachable webhook...")
	err := publishChange(webhook, float32(10))
	if err != nil {
		t.Error(err)
	}

	webhook2 := Webhook{"testID", "testURL", "EUR", "NOK",
		float32(8), float32(9)}

	fmt.Println("Invoking not reachable webhook...")
	err2 := publishChange(webhook2, float32(10))
	if err2 == nil {
		t.Error(errors.New("Expected error from invoking webhook with no existing URL"))
	} else {
		fmt.Println("Correct. Invoking not reachable webhook returned error: " + err2.Error())
	}

}

// help function to handle invoke webhook on a test server
func handleInvoveTest(w http.ResponseWriter, r *http.Request) {
	expectedPayload := InvokePayload{"EUR", "NOK",
		float32(10), float32(8), float32(9)}

	var updatePayload InvokePayload

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			http.Error(w, "Error reading request body", 400)
		}
		json.Unmarshal(JSONdata, &updatePayload)
		// when here it is succesfully notified, checkig received payload with pattern
		// to avoid double requst to ticker, assume that currentRate is outside 5-5.5 and test it manually

		if updatePayload.BaseCurrency != expectedPayload.BaseCurrency ||
			updatePayload.TargetCurrency != expectedPayload.TargetCurrency ||
			updatePayload.MinTriggerValue != expectedPayload.MinTriggerValue ||
			updatePayload.MaxTriggerValue != expectedPayload.MaxTriggerValue ||
			(updatePayload.CurrentRate > updatePayload.MinTriggerValue &&
				updatePayload.CurrentRate < updatePayload.MaxTriggerValue) {

			http.Error(w, "Not correct payload format", 400)
		}

	} else {
		http.Error(w, "Not correct method, use POST to notify webhook", 400)
	}
}

func TestHandleAverageRate(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(HandleAverageRate))

	// EUR -> NOK

	avgPayload := LatestPayload{"EUR", "NOK"}

	jsonStr, _ := json.Marshal(avgPayload)

	res, err := http.Post(ts.URL, "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Error(err)
		return
	}
	respData, err2 := ioutil.ReadAll(res.Body)
	if err2 != nil {
		t.Error(err2)
		return
	}
	res.Body.Close()

	avgRateEurNok, err := strconv.ParseFloat(string(respData), 64)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf("\nAverage EUR->NOK as float32: %f\n", float32(avgRateEurNok))

	// NOK -> EUR

	avgPayload2 := LatestPayload{"NOK", "EUR"}

	jsonStr2, _ := json.Marshal(avgPayload2)

	res2, err3 := http.Post(ts.URL, "application/json", bytes.NewBuffer(jsonStr2))
	if err3 != nil {
		t.Error(err3)
		return
	}
	respData2, err4 := ioutil.ReadAll(res2.Body)
	if err4 != nil {
		t.Error(err4)
		return
	}
	res2.Body.Close()

	avgRateNokEur, err5 := strconv.ParseFloat(string(respData2), 64)
	if err5 != nil {
		t.Error(err5)
		return
	}

	fmt.Printf("Average NOK->EUR as float32: %f\n", float32(avgRateNokEur))

	// PLN -> NOK

	avgPayload3 := LatestPayload{"PLN", "NOK"}

	jsonStr3, _ := json.Marshal(avgPayload3)

	res3, err6 := http.Post(ts.URL, "application/json", bytes.NewBuffer(jsonStr3))
	if err6 != nil {
		t.Error(err6)
		return
	}
	respData3, err7 := ioutil.ReadAll(res3.Body)
	if err7 != nil {
		t.Error(err7)
		return
	}
	res3.Body.Close()

	avgRatePlnNok, err8 := strconv.ParseFloat(string(respData3), 64)
	if err8 != nil {
		t.Error(err8)
		return
	}

	fmt.Printf("Average PLN->NOK as float32: %f\n", float32(avgRatePlnNok))

	// incorrect currency codes

	avgPayload4 := LatestPayload{"xxx", "yyy"}

	jsonStr4, _ := json.Marshal(avgPayload4)

	res4, err9 := http.Post(ts.URL, "application/json", bytes.NewBuffer(jsonStr4))
	if err9 != nil {
		t.Error(err9)
		return
	}

	if res4.StatusCode != http.StatusBadRequest {
		t.Error(errors.New("Not received expected error 400 from HandleAverage"))
	} else {
		fmt.Println("Received expected code 400 for incorrect rate payload {xxx,yyy}")
	}
}

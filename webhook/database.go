package webhook

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

var dbURL = "mongodb://cloud_p2:kjh67_u@ds237445.mlab.com:37445/p2"

//var dbURL = os.Getenv("DB")

func getLatestRatesFromDB() (Rates, error) {
	var latest Rates
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return latest, err
	}
	defer session.Close()

	var results []Rates
	err = session.DB("p2").C("rates").Find(bson.M{}).Limit(1).Sort("-timestamp").All(&results)
	if err != nil {
		return latest, err
	}
	if len(results) > 0 { // OK if not empty result
		latest = results[0]
	}
	return latest, nil // never updated before
}

func getRecentSevenRatesFromDB() ([]Rates, error) {
	var recentSeven []Rates
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return recentSeven, err
	}
	defer session.Close()

	err = session.DB("p2").C("rates").Find(bson.M{}).Limit(7).Sort("-timestamp").All(&recentSeven)
	if err != nil {
		return recentSeven, err
	}

	return recentSeven, nil // never updated before
}

func addLatestRatesToDB(currentRates Rates) (string, error) {

	session, err1 := mgo.Dial(dbURL)
	if err1 != nil {
		//fmt.Println(err2)
		return "", err1
	}
	defer session.Close()

	// try to insert into db
	err2 := session.DB("p2").C("rates").Insert(currentRates)
	if err2 != nil {
		return "", err2
	}
	// succesfully updated

	return "Added newest rates in db", nil
}

func getAllWebhooksFromDb() ([]Webhook, error) {
	var currentWebhooks []Webhook
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return currentWebhooks, err
	}
	defer session.Close()

	err2 := session.DB("p2").C("webhooks").Find(bson.M{}).All(&currentWebhooks)
	if err2 != nil {
		return currentWebhooks, err2
	}
	return currentWebhooks, nil
}

func insertWebhookIntoDb(webhook Webhook) bool {
	if len(webhook.WebhookURL) == 0 {
		return false
	}
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return false
	}
	defer session.Close()

	err2 := session.DB("p2").C("webhooks").Insert(webhook)
	session.DB("p2").C("insert_log").Insert(webhook)
	if err2 != nil {
		return false
	}
	return true
}

func getWebhookFromDatabase(webhookID string) (Webhook, error) {
	var toReturn Webhook
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return toReturn, err
	}
	defer session.Close()

	var queryResults []Webhook

	err2 := session.DB("p2").C("webhooks").Find(bson.M{"id": webhookID}).All(&queryResults)
	if err2 != nil {
		return toReturn, err2
	}

	if len(queryResults) > 0 { // found - sending first match, to do response to multiply results?
		toReturn.ID = queryResults[0].ID
		toReturn.WebhookURL = queryResults[0].WebhookURL
		toReturn.MinTriggerValue = queryResults[0].MinTriggerValue
		toReturn.MaxTriggerValue = queryResults[0].MaxTriggerValue
		toReturn.BaseCurrency = queryResults[0].BaseCurrency
		toReturn.TargetCurrency = queryResults[0].TargetCurrency
		return toReturn, nil
	}
	err3 := errors.New("Received multiply results on ID query !!")
	return toReturn, err3
}

func removeWebhookData(webhookID string) error {
	session, err := mgo.Dial(dbURL)
	if err != nil {
		return err
	}
	defer session.Close()

	_, err3 := getWebhookFromDatabase(webhookID)
	if err3 != nil {
		return err3
	}

	err2 := session.DB("p2").C("webhooks").Remove(bson.M{"id": webhookID})

	if err2 != nil {
		return err
	}
	return nil
}

func getLatestRate(baseCurrency string, targetCurrency string) (float32, error) {
	latestRates, err1 := getLatestRatesFromDB()
	if err1 != nil {
		return float32(0), err1 // feil while getting latest rates from db
	}
	if strings.Compare(baseCurrency, "EUR") == 0 {
		// EUR -> x return rate
		temp := latestRates.RatesList[targetCurrency]
		if temp > 0 {
			return temp, nil
		}
		return float32(0), errors.New("Unknown target currency")
	}
	if strings.Compare(targetCurrency, "EUR") == 0 {
		// x -> EUR return 1/rate
		temp := latestRates.RatesList[baseCurrency]
		if temp > 0 {
			return 1 / temp, nil
		}
		return float32(0), errors.New("Unknown base currency")
	}
	// x -> y
	base := latestRates.RatesList[baseCurrency]
	target := latestRates.RatesList[targetCurrency]

	if base > 0 && target > 0 {
		return target / base, nil
	}

	return float32(0), errors.New("Unknown currencies")
}

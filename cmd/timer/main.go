package main

import (
	"bitbucket.org/TomaszRudowski/cloud_p2/webhook"
	"fmt"
	"time"
)

func main() {
	// sleep 24 t
	// run webhook.updateRates()		// this test and skip inserting of multiply records if run more the once a day
	// based on github.com/marni/student_db

	delay := time.Minute * 29

	for {
		fmt.Printf("\nRunning autoupdate\n")
		msg, err := webhook.UpdateRates()
		fmt.Println(msg)
		if err != nil {
			fmt.Println(err.Error())
		}
		for i := 1; i < 50; i++ {
			time.Sleep(delay)
			fmt.Printf("Loop no.%d", i)
		}

	}
}
